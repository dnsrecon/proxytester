FROM ubuntu:latest
MAINTAINER erratic@yourstruly.sx

RUN apt-get update && apt-get -y install cron parallel curl

# Install ProxyScanner
ADD ProxyScanner.sh /etc/cron.hourly/ProxyScanner.sh

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.hourly/ProxyScanner.sh

# Run the command on container startup
CMD cron -f -L 15